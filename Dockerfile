FROM registry.access.redhat.com/ubi8/ubi
RUN rm -f /etc/yum.repos.d/ubi.repo
ADD RHEL-8-appstream.repo /etc/yum.repos.d/RHEL-8-appstream.repo
ADD RHEL-8-baseos.repo /etc/yum.repos.d/RHEL-8-baseos.repo
ADD yum.conf /etc/yum.conf
ADD mlc_internal_avx512 /home/mlc_internal_avx512
ADD stream_omp_NTW_avx3 /home/stream_omp_NTW_avx3
ADD docker_stream.sh /home/docker_stream.sh
ADD MLC.sh /home/MLC.sh
RUN mkdir -p /home/spec17
ADD cpu2017-1.0.2.iso /home/spec17
ADD l_mklb_p_11.3.3.011.tgz /home/linpack
ADD l_mpi_2018.1.163.tgz /home/linpack
COPY FOR-INTEL-cpu2017-1.0.2-ic18.0-lin-binaries-20170901.tar.xz /home/spec17/FOR-INTEL-cpu2017-1.0.2-ic18.0-lin-binaries-20170901.tar.xz
ENV HTTP_PROXY http://child-prc.intel.com:913
ENV HTTPS_PROXY https://child-prc.intel.com:913
ENV http_proxy http://child-prc.intel.com:913
ENV https_proxy https://child-prc.intel.com:913
CMD ["cat /etc/resolv.conf"]
CMD ["env | grep -i proxy"]
RUN yum --assumeyes update
RUN yum --assumeyes install numactl
RUN yum --assumeyes install vim
RUN yum --assumeyes install xz
RUN yum --assumeyes install cpio
RUN mkdir -p /home/spec17/isomount
RUN ln -s /usr/lib64/libnsl.so.2 /usr/lib64/libnsl.so.1
RUN sed -i 's/ACCEPT_EULA=decline/ACCEPT_EULA=accept/g' /home/linpack/l_mpi_2018.1.163/silent.cfg
RUN ./home/linpack/l_mpi_2018.1.163/install.sh --INSTALL_MODE=NONRPM -s /home/linpack/l_mpi_2018.1.163/silent.cfg
WORKDIR /home/
CMD ["/bin/bash"]
